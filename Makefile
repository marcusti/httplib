.PHONY: clean cover format init test tidy update version

GOPATH=$(shell go env GOPATH)

clean:
	rm -rf ./bin ./tmp ./vendor go.sum
	mkdir -p ./tmp

cover: test
	go tool cover -html=./tmp/cover.out

format: ${GOPATH}/bin/gofumpt
	gofumpt -w .
	go vet ./...

init:
	mkdir -p bin

test: tidy format
	go test -coverprofile ./tmp/cover.out -p 1 -v ./...
	go tool cover -func=./tmp/cover.out

tidy:
	go mod tidy

update:
	go get -u ./...

version:
	go version

${GOPATH}/bin/gofumpt:
	go install mvdan.cc/gofumpt@latest
