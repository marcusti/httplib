package httplib_test

import (
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/marcusti/httplib"
)

type Map map[string]interface{}

func TestServerStartAndShutdown(t *testing.T) {
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		httplib.RespondText(w, r, http.StatusOK, "OK")
	})
	s := httplib.NewServer("127.0.0.1", 9000, nil)
	c := httplib.NewSignalsChannel()
	baseURL := "http://" + s.Addr

	go func() {
		assert.NoError(t, httplib.StartServer(s, c))
	}()
	time.Sleep(time.Millisecond * 50)

	res, err := http.Get(baseURL + "/ping")
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)

	c <- os.Interrupt
	time.Sleep(time.Millisecond * 50)

	_, err = http.Get(baseURL + "/ping")
	assert.ErrorContains(t, err, "connection refused")
}

func TestServerStartInvalidHost(t *testing.T) {
	s := httplib.NewServer("xxx", 0, nil)
	c := httplib.NewSignalsChannel()
	err := httplib.StartServer(s, c)
	assert.ErrorContains(t, err, "error starting HTTP server")
	assert.ErrorContains(t, err, "no such host")
}

func TestDecode(t *testing.T) {
	type request struct {
		Foo string `json:"foo"`
	}
	data := &request{}
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/", strings.NewReader(`{"foo":"bar"}`))
	assert.NoError(t, httplib.Decode(w, r, data))
	assert.Equal(t, "bar", data.Foo)
}

func TestRespond(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	assert.NoError(t, httplib.Respond(w, r, http.StatusOK, Map{"foo": "bar"}))
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, httplib.ContentTypeApplicationJSON, w.Header().Get(httplib.ContentType))
	assert.Equal(t, `{"foo":"bar"}`, strings.TrimSpace(w.Body.String()))
}

func TestRespondBadRequest(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	assert.NoError(t, httplib.Respond(w, r, http.StatusBadRequest, Map{"foo": "bar"}))
	assert.Equal(t, http.StatusBadRequest, w.Code)
	assert.Equal(t, httplib.ContentTypeApplicationJSON, w.Header().Get(httplib.ContentType))
	assert.Equal(t, `{"foo":"bar"}`, strings.TrimSpace(w.Body.String()))
}

func TestRespondJSON(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	assert.NoError(t, httplib.RespondJSON(w, r, http.StatusOK, Map{"foo": "bar"}))
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, httplib.ContentTypeApplicationJSON, w.Header().Get(httplib.ContentType))
	assert.Equal(t, `{"foo":"bar"}`, strings.TrimSpace(w.Body.String()))
}

func TestRespondJSONIndent(t *testing.T) {
	r := httptest.NewRequest("GET", "/?pretty", nil)
	w := httptest.NewRecorder()
	assert.NoError(t, httplib.RespondJSON(w, r, http.StatusOK, Map{"foo": "bar"}))
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, httplib.ContentTypeApplicationJSON, w.Header().Get(httplib.ContentType))
	assert.Equal(t, "{\n  \"foo\": \"bar\"\n}", strings.TrimSpace(w.Body.String()))
}

func TestJSONError(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	assert.Error(t, httplib.RespondJSON(w, r, http.StatusOK, make(chan int)))
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestRespondText(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	assert.NoError(t, httplib.RespondText(w, r, http.StatusOK, "foo"))
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, httplib.ContentTypeTextPlain, w.Header().Get(httplib.ContentType))
	assert.Equal(t, "foo", w.Body.String())
}

func TestRespondTextBadRequest(t *testing.T) {
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	assert.NoError(t, httplib.RespondText(w, r, http.StatusBadRequest, "foo"))
	assert.Equal(t, http.StatusBadRequest, w.Code)
	assert.Equal(t, httplib.ContentTypeTextPlain, w.Header().Get(httplib.ContentType))
	assert.Equal(t, "foo", w.Body.String())
}

func TestHandle(t *testing.T) {
	f := func(w http.ResponseWriter, r *http.Request) error {
		return httplib.RespondText(w, r, http.StatusOK, "foo")
	}
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	httplib.Handle(f)(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, httplib.ContentTypeTextPlain, w.Header().Get(httplib.ContentType))
	assert.Equal(t, "foo", w.Body.String())
}

func TestHandleError(t *testing.T) {
	f := func(w http.ResponseWriter, r *http.Request) error {
		return errors.New("test error")
	}
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	httplib.Handle(f)(w, r)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	assert.Equal(t, httplib.ContentTypeTextPlain, w.Header().Get(httplib.ContentType))
	assert.Equal(t, "test error", strings.TrimSpace(w.Body.String()))
}

func TestGetQueryParam(t *testing.T) {
	data := []struct {
		target   string
		expected string
	}{
		{"/", ""},
		{"/?s=foo", "foo"},
		{"/?s=%20foo", "foo"},
	}
	for _, d := range data {
		r := httptest.NewRequest("GET", d.target, nil)
		val := httplib.GetQueryParam(r, "s", "")
		assert.Equal(t, d.expected, val)
	}
}

func TestGetIntQueryParam(t *testing.T) {
	data := []struct {
		target   string
		expected int
	}{
		{"/", 5},
		{"/?foo=bar", 5},
		{"/?max=bar", 5},
		{"/?max=-1", -1},
		{"/?max=0", 0},
		{"/?max=1", 1},
	}
	for _, d := range data {
		r := httptest.NewRequest("GET", d.target, nil)
		max := httplib.GetIntQueryParam(r, "max", 5)
		assert.Equal(t, d.expected, max)
	}
}
