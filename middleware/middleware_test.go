package middleware

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRouter(t *testing.T) {
	h := NewRouter()
	h.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	})
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestRouterPanic(t *testing.T) {
	h := NewRouter()
	h.Get("/", func(w http.ResponseWriter, r *http.Request) {
		panic(fmt.Errorf("test panic"))
	})
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)
	h.ServeHTTP(w, r)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}
