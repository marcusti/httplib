package middleware

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
)

// NewRouter returns a new HTTP route multiplexer with a default middleware stack.
func NewRouter() *chi.Mux {
	r := chi.NewRouter()

	r.Use(hlog.NewHandler(log.Logger))
	r.Use(hlog.MethodHandler("method"))
	r.Use(hlog.URLHandler("url"))
	r.Use(hlog.ProtoHandler("protocol"))
	r.Use(hlog.RefererHandler("referer"))
	r.Use(hlog.UserAgentHandler("agent"))
	r.Use(hlog.RequestIDHandler("requestID", "Request-ID"))
	r.Use(hlog.AccessHandler(func(r *http.Request, status, size int, duration time.Duration) {
		hlog.FromRequest(r).Info().
			Int("status", status).
			Int("size", size).
			Dur("durationValue", duration).
			Str("duration", formatDuration(duration)).
			Msg("")
	}))

	r.Use(middleware.CleanPath)
	r.Use(middleware.StripSlashes)
	r.Use(Recoverer)
	return r
}

// Recoverer recovers from panics, logs the panic and returns a HTTP 500.
// Inspired by chi.Recoverer
func Recoverer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil && rec != http.ErrAbortHandler {

				log.Error().
					Str("panic", fmt.Sprintf("%v", rec)).
					Msg("recovered from panic")

				w.WriteHeader(http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	})
}

func formatDuration(d time.Duration) string {
	return fmt.Sprintf("%s", d)
}
