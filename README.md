# httplib

Go HTTP library.

Dependencies (see [go.mod](/go.mod)):

- [errors](https://github.com/pkg/errors) for error wrapping
- [testify](https://github.com/stretchr/testify) for testing
- [zerolog][zerolog] for structured logging

Middleware dependencies:

- [chi](https://github.com/go-chi/chi) for routing and middleware

## package `middleware`

Provides a request logger middleware based on [zerolog][zerolog].

[zerolog]: https://github.com/rs/zerolog/
