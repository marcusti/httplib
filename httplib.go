package httplib

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

const (
	// ContentType content type HTTP header key
	ContentType = "Content-Type"

	// ContentTypeApplicationJSON JSON content type HTTP header value
	ContentTypeApplicationJSON = "application/json; charset=utf-8"

	// ContentTypeTextPlain text content type HTTP header value
	ContentTypeTextPlain = "text/plain; charset=utf-8"

	// PrettyParameter URL parameter for pretty printing JSON
	PrettyParameter = "pretty"
)

type (
	// HandlerFunc is a http.HandlerFunc that returns an error
	HandlerFunc func(w http.ResponseWriter, r *http.Request) error

	httpError struct {
		Code    int    `json:"code"`
		Status  string `json:"status"`
		Message string `json:"message"`
	}

	errorResponse struct {
		Error httpError `json:"error"`
	}
)

// NewServer returns a default HTTP server.
func NewServer(host string, port uint16, handler http.Handler) *http.Server {
	return &http.Server{
		Addr:         fmt.Sprintf("%s:%d", host, port),
		Handler:      handler,
		ReadTimeout:  time.Second * 20,
		WriteTimeout: time.Second * 20,
		IdleTimeout:  time.Second * 20,
	}
}

// NewSignalsChannel creates a new channel for interrupt signals.
func NewSignalsChannel() chan os.Signal {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	return c
}

// StartServer start an HTTP server.
func StartServer(s *http.Server, c chan os.Signal) error {
	// listen to shutdown signals on a new goroutine
	go func() {
		<-c

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
		defer func() {
			cancel()
		}()

		log.Info().Msg("shutting down HTTP server")
		if err := s.Shutdown(ctx); err != nil {
			log.Panic().Err(err).Msg("error shutting down HTTP server")
		}
	}()

	// start the server
	log.Info().Str("URL", "http://"+s.Addr).Msg("starting HTTP server")
	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		return errors.Wrap(err, "error starting HTTP server")
	}
	return nil
}

// Handle converts a Handler into a standard http.HandlerFunc
func Handle(h HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := h(w, r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set(ContentType, ContentTypeTextPlain)
			w.Write([]byte(err.Error()))
		}
	}
}

// Decode decodes HTTP request data.
func Decode(w http.ResponseWriter, r *http.Request, v interface{}) error {
	return json.NewDecoder(r.Body).Decode(v)
}

// Respond encodes HTTP response data.
func Respond(w http.ResponseWriter, r *http.Request, code int, i interface{}) error {
	return RespondJSON(w, r, code, i)
}

// RespondJSON encodes HTTP response data as JSON.
// Indentation can be controlled by a url query parameter 'pretty'.
func RespondJSON(w http.ResponseWriter, r *http.Request, code int, i interface{}) error {
	indent := ""
	if r.URL.Query().Has(PrettyParameter) {
		indent = "  "
	}

	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(true)
	enc.SetIndent("", indent)
	if err := enc.Encode(i); err != nil {
		return err
	}

	w.Header().Set(ContentType, ContentTypeApplicationJSON)
	w.WriteHeader(code)
	_, err := w.Write(buf.Bytes())
	return err
}

// RespondText encodes HTTP response data as plain text.
func RespondText(w http.ResponseWriter, r *http.Request, code int, s string) error {
	w.Header().Set(ContentType, ContentTypeTextPlain)
	w.WriteHeader(code)
	_, err := w.Write([]byte(s))
	return err
}

// GetQueryParam returns a trimmed request URL query parameter or a default value.
func GetQueryParam(r *http.Request, key, defaultValue string) string {
	val := strings.TrimSpace(r.URL.Query().Get(key))
	if val == "" {
		return defaultValue
	}
	return val
}

// GetIntQueryParam returns a request URL query parameter as an int or a default value.
func GetIntQueryParam(r *http.Request, key string, defaultValue int) int {
	s := r.URL.Query().Get(key)
	value, err := strconv.Atoi(s)
	if err != nil {
		value = defaultValue
	}
	return value
}
